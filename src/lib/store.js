import { configureStore } from '@reduxjs/toolkit';
import voteReducer from './features/vote/voteSlice';

export default configureStore({
    reducer: {
        vote: voteReducer,
    },
});